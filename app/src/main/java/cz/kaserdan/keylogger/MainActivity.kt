package cz.kaserdan.keylogger

import android.content.ComponentName
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        val serviceEnabled = isAccessibilityServiceEnabled()
        status_tv.text = if (serviceEnabled) "ENABLED" else "DISABLED"
        status_tv.setTextColor(if (serviceEnabled) Color.GREEN else Color.RED)
        activate_btn.isVisible = !serviceEnabled
        activate_btn.setOnClickListener { startActivity(Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)) }
    }

    private fun isAccessibilityServiceEnabled(): Boolean {
        val expectedComponentName = ComponentName(this, KeyLoggerService::class.java)

        val enabledServicesSetting = Settings.Secure.getString(
            contentResolver,
            Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES
        ) ?: return false

        return enabledServicesSetting.split(":")
            .any { ComponentName.unflattenFromString(it) == expectedComponentName }
    }

}